Changelog
=========

- import ligo.skymap instead of using a _vendor/ligo/skymap/util version.

v1.4.8 Released
-----------------

- Most of the functionality implemented
